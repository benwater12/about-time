## 0.1.68
* Added named years support. Internally all calculations are done with numeric years. On output a given year number is converted to a year name assuming year names cycle starting at 0.
* Added support for year names in calendar editor.
* See Readme.md for example usage.
* Added sample calendar's for Dark Sun and Nehwon.
* Updated es.json - thanks Jose Lozano

